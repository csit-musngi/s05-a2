<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register Confirmation</title>
</head>
<body>
<h1>Registration Confirmation</h1>
	<p>First Name: <%= session.getAttribute("fname") %></p>
	<p>Last Name: <%= session.getAttribute("lname") %></p>
	<p>Phone Number: <%= session.getAttribute("phone") %></p>
	<p>Email: <%= session.getAttribute("email") %></p>
	<p>App Discovery: <%= session.getAttribute("discover") %></p>
	<p>Date of Birth: <%= session.getAttribute("birth") %></p>
	<p>User Type: <%= session.getAttribute("choice") %></p>
	<p>Description: <%= session.getAttribute("description") %></p>
	
	<form action="login" method="post">
			<input type="submit">
		</form>
	
	<form action = "index.jsp">
	<input type="submit" value="Back">
	</form>
</body>
</html>