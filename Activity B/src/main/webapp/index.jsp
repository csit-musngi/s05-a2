<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register JobFinder</title>
<style>
div{
		margin-top: 5px;
		margin-bottom: 5px;
	}

</style>
</head>
<body>
<form action="register" method="post">
<h1>Welcome to Servlet Job Finder!</h1>

<div>
<label for="fname">First Name</label>
<input type="text" name="fname" required>
<br>
</div> 
<div>
<label for="lname">Last Name</label>
<input type="text" name="lname" required>
<br>
</div>
<div>
<label for="phone">Phone</label>
<input type="text" name="phone" required>
<br>
</div>
<div>
<label for="email">Email</label>
<input type="text" name="email" required>
<br>
</div>

<fieldset>
<legend>Which vehicle do you require?</legend>
<input type="radio" id="friend" name="discover" value="friend" required>
<label for="friend">Friend</label>
<br>
<input type="radio" id="social" name="discover" value="social" required>
<label for="social">Social Media</label>
<br>					
<input type="radio" id="other" name="discover" value="other" required>
<label for="other">Others</label>
</fieldset>

<div>
<label for ="birth">Date of Birth</label>
<input type="date" name="birth" required>
</div>

<div>
<label for="choice">Are you an employer or applicant?</label>
<select id="combo" name="choice">
	<option value="applicant">Applicant</option>
	<option value="employer">Employer</option>
</select>
</div>

<div>
<label for="description">Profile Description</label>
<textarea name="description" maxlength="500"></textarea>
</div>

<button>Register</button>	
</form>


</body>
</html>