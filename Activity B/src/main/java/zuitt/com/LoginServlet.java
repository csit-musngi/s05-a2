package zuitt.com;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8929692527952926626L;
	
	
	public void init() throws ServletException {
		System.out.println("............................");
		System.out.println("Login Servlet connection started.");
		System.out.println("............................");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		HttpSession session = req.getSession();
		
		String fullname = session.getAttribute("fname").toString()+ " " +session.getAttribute("lname").toString();
		String type = session.getAttribute("choice").toString();
		
		session.setAttribute("fullname", fullname);
		session.setAttribute("type", type);
		
		res.sendRedirect("home.jsp");
	}
	
	public void destroy() {
		System.out.println("............................");
		System.out.println("Login Servlet connection destroyed.");
		System.out.println("............................");
	}

}
