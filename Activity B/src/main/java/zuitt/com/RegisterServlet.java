package zuitt.com;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7033598641401140455L;
	
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String discover = req.getParameter("discover");
		String birth = req.getParameter("birth");
		String choice = req.getParameter("choice");
		String description = req.getParameter("description");
		
		// Store all the data from the form into the session
		HttpSession session = req.getSession();
		
		session.setAttribute("fname", fname);
		session.setAttribute("lname", lname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("discover", discover);
		session.setAttribute("birth", birth);
		session.setAttribute("choice", choice);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp");
	}
	public void destroy() {
		System.out.println("RegisterServlet has been finalized.");
	}

}
